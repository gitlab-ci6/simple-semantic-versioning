# [2.0.0](https://gitlab.com/gitlab-ci6/simple-project/compare/v1.0.3...v2.0.0) (2022-04-29)


### Bug Fixes

* update version ([c0eff2d](https://gitlab.com/gitlab-ci6/simple-project/commit/c0eff2de22d0f982844913a28ca1d9128ffb7377))


### BREAKING CHANGES

* update major version

## [1.0.3](https://gitlab.com/gitlab-ci6/simple-project/compare/v1.0.2...v1.0.3) (2022-04-27)


### Bug Fixes

* add assets ([ecefa57](https://gitlab.com/gitlab-ci6/simple-project/commit/ecefa578a5871d57e9c59dbf3ca0cc9334703189))

## [1.0.2](https://gitlab.com/gitlab-ci6/simple-project/compare/v1.0.1...v1.0.2) (2022-04-27)


### Bug Fixes

* add changelog plugin ([09c7fa2](https://gitlab.com/gitlab-ci6/simple-project/commit/09c7fa243ba2b835f6677323d200cc073459a20f))
